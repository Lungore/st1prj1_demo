﻿using System;
using System.Globalization;
using RaspberryPi;

namespace ST1PRJ1_demo
{
    public class PulseReader
    {
        private readonly Puls _puls;
        private DateTime _startTime;

        public PulseReader(RaspberryPiDll raspberryPi)
        {
            _puls = new Puls(raspberryPi);
        }

        public void StartReading()
        {
            _puls.StartPuls();
            _startTime = DateTime.Now;
        }

        public Reading ReadPulse()
        {
            var numberOfPulses = _puls.ReadPuls();
            var stopTime = DateTime.Now;
            var reading = new Reading()
            {
                NumberOfPulses = Convert.ToString(numberOfPulses),
                CalculatedPulse = Convert.ToString(CalculatePulse(numberOfPulses, 
                    _startTime, stopTime), CultureInfo.CurrentCulture),
                StartTime = _startTime.ToLongTimeString(),
                StopTime = stopTime.ToLongTimeString()
            };

            return reading;
        }

        private static double CalculatePulse(int numberOfPulses, DateTime startTime, DateTime stopTime)
        {
            var minutesElapsed = stopTime.Subtract(startTime).TotalMinutes;

            return Convert.ToDouble(numberOfPulses) / minutesElapsed;
        }
    }
}