﻿namespace ST1PRJ1_demo
{
    public class Reading
    {
        public string Id { get; set; }
        public string StartTime { get; set; }
        public string StopTime { get; set; }
        public string NumberOfPulses { get; set; }
        public string CalculatedPulse { get; set; }

        public override string ToString()
        {
            return "Id: " + Id + " StartTime: " + StartTime + " StopTime: " + StopTime
                   + " Number of pulses: " + NumberOfPulses + " Calculated pulse: " + CalculatedPulse;
        }
    }
}