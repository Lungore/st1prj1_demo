﻿using System;

namespace ST1PRJ1_demo
{
    public class BcdConverter
    {
        public static byte Convert(int num)
        {
            if (0 > num && num > 99) throw new ArgumentException("Number out of range", nameof(num));
            return (byte)((num / 10) << 4 | (num % 10));
            //var ones = (byte)(num % 10);
            //var tens = (byte)(num / 10);
            //return (byte)(tens << 4 | ones);
        }
    }
}