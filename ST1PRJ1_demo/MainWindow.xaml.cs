﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RaspberryPi;

namespace ST1PRJ1_demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly RaspberryPiDll _rpi;
        private readonly PulseReader _pulseReader;
        private readonly RaspberryPi.Button _button;
        private ReadingState _readingState;
        private DateTime _lastButtonPress;
        private readonly PWM _pwm;
        private readonly SevenSeg _sevenSeg;

        private enum ReadingState
        {
            Stopped,
            Started
        }


        public MainWindow()
        {
            _rpi = new RaspberryPiDll();
            _pulseReader = new PulseReader(_rpi);
            _button = new RaspberryPi.Button(_rpi, RaspberryPi.Button.KEYS.P1);
            _readingState = ReadingState.Stopped;
            _lastButtonPress = DateTime.Now;
            _pwm = new PWM(_rpi);
            _sevenSeg = new SevenSeg(_rpi);

            InitializeComponent();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_rpi.Open())
            {
                MessageBox.Show("Error opening connection to Raspberry Pi.\r\nProgram stops now.", "ERROR:");
                System.Environment.Exit(1);
            }

            _pwm.InitPWM(0);
            _sevenSeg.Init_SevenSeg();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var shouldStop = false;
            _readingState = ReadingState.Stopped;

            Button1.IsEnabled = false;
            Button1.Background = Brushes.Chartreuse;

            _pwm.SetPWM(50);

            while (!shouldStop)
            {
                if (_button.KeyPressed == 1 && DateTime.Now.Subtract(_lastButtonPress).Seconds >= 1)
                {
                    _lastButtonPress = DateTime.Now;
                    switch (_readingState)
                    {
                        case ReadingState.Stopped:
                            _pulseReader.StartReading();
                            _readingState = ReadingState.Started;
                            _sevenSeg.Disp_SevenSeg(0x00);
                            _pwm.SetPWM(100);
                            break;

                        case ReadingState.Started:
                            var reading = _pulseReader.ReadPulse();
                            DataGrid1.Items.Add(reading);
                            _readingState = ReadingState.Stopped;
                            var calculatedPulse = reading.CalculatedPulse;
                            var calculatedPulse2 = Convert.ToDouble(calculatedPulse);
                            _sevenSeg.Disp_SevenSeg(BcdConverter.Convert((int)Math.Round(calculatedPulse2, 0)));
                            _pwm.SetPWM(10); 
                            shouldStop = true;
                            break;
                    }
                }
            }

            Button1.IsEnabled = true;
            Button1.Background = SystemColors.ControlLightBrush;
        }
    }
}
